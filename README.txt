
AutomaticMenu

Automatically create menu items based on content types.

Having menus items correctly inserted into the menu hierarchy has a
variety of benefits including making sure the breadcrumb trail works,
and the active menu trail works.

Installation
------------

Copy automaticmenu.module to your module directory and then enable on the admin
modules page. Configure menu items to be created from the
admin/setting/automaticmenu page.

Author
------
Richard Perrin, Lupus Studios Ltd
contact@lupusstudios.com
http://www.lupusstudios.com